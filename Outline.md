- Widget
- Input location
  - Get current location
  - Input alternate location
- Display weather data



### Design Inspiration

https://dribbble.com/shots/1081917-WhereTO-App

https://dribbble.com/shots/1352357-US-City-Widget-Chicago-NY-Philadelphia

https://dribbble.com/shots/2526900-Weather-App

### Widget

https://github.com/karol-f/vue-custom-element





### Unsplash

https://github.com/unsplash/unsplash-js

- access key: e9bebf20d72dcf9ea30c1380cf3d34dccd09825fa3878090d1ea2740441c2de4

- secret key: c7204c5b70de5b8b6227a135c1a9ae38f904d9580f9486d97730113c1196ba8c

  

#### **OpenWeather**

- api key: 7da893729634bffd1ed114300542372b

**Attributes**

- q={city},{country-code}
- zip={zip},{country-code}
- lat={lat}&lon={lon}
- 

**Response Format**

```json
{
    "coord": {
        "lon": -122.09,
        "lat": 37.39
    },
    "weather": [{
        "id": 500,
        "main": "Rain",
        "description": "light rain",
        "icon": "10d"
    }],
    "base": "stations",
    "main": {
        "temp": 280.44,
        "pressure": 1017,
        "humidity": 61,
        "temp_min": 279.15,
        "temp_max": 281.15
    },
    "visibility": 12874,
    "wind": {
        "speed": 8.2,
        "deg": 340,
        "gust": 11.3
    },
    "clouds": {
        "all": 1
    },
    "dt": 1519061700,
    "sys": {
        "type": 1,
        "id": 392,
        "message": 0.0027,
        "country": "US",
        "sunrise": 1519051894,
        "sunset": 1519091585
    },
    "id": 0,
    "name": "Mountain View",
    "cod": 200
}
```



fixed set of searches

- sunny
- cloudy
- partly cloudy
- light rain
- heavy rain
- light snow
- heavy snow

# todo

- :ballot_box_with_check: fix linting errors & warnings
- :ballot_box_with_check: polling every 60s
- :ballot_box_with_check: write test suite:
  - renders with prop
  - prop units
  - prop zip
  - prop country code
  - geolocation
  - manually enter zip
  - manually enter country
- :ballot_box_with_check: write readme.html
- deploy to git repo and send email with ime tracking