# weather-widget

Widget to display current weather conditions for a given geolocation or zip code


## Deployment

### Run on your site
- Load the dist app.css file, app.js file and add the custom html element `<vue-widget></vue-widget>`
```
<link href="https://yourdomain.com/css/app.css" rel="stylesheet"/>
<vue-widget></vue-widget>
<script src="http://yourdomain.com/js/app.js"></script>
```

### Available element properties
```<vue-widget units="imperial" initialZip="02452" initialCountryCode="US"></vue-widget>```

- **units**: 'imperial', 'celcius', 'kelvin'
- **initialZip**: any valid zip code
- **initialCountryCode**: any valid 2-letter country code
- **updateFrequency**: time in ms between auto updates of weather data

## Project setup

- Sign up for api keys from openweathermap.org and unsplash.com
- Copy `.env.example` to `.env.local` and fill in your api keys
- run `yarn install` to install dependencies

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run your end-to-end tests
```
yarn run test:e2e
```
