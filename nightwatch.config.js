const chromedriverPath = require('chromedriver').path;

module.exports = {
  selenium: {
    cli_args: {
      'webdriver.chrome.driver': chromedriverPath,
    },
  },
};
