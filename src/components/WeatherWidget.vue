<template>
  <div class="widget"
       :style="current_weather_style"
       @mouseover="input_focused=true"
       @mouseleave="input_focused=false">

    <div class="loader" v-show="loading">
      <font-awesome-icon icon="spinner" class="loader--spinning" size="4x"/>
    </div>

    <div class="widget__error" v-show="error">
      {{ error }}
    </div>

    <div class="widget__input"
        :class="{'widget__input--focused': input_focused}">
      <font-awesome-icon icon="map-marker-alt"
                         class="widget__input__geo"
                         @click="updateWeatherGeolocation"
                         v-if="geolocation_supported" />
      <div class="widget__input__zip">
        <input type="text"
               class="widget__input__zip__element"
               placeholder="Zip Code"
               size="8"
               v-model="zip"
               @keyup.enter="updateWeatherZip"/>
        <select v-model="country"
                @change="updateWeatherZip"
                class="widget__input__zip__element">
          <option v-for="country in countries"
                  :value="country.abbreviation"
                  :key="country.country">
           {{country.abbreviation}}
          </option>
        </select>
      </div>
    </div>

    <div class="widget__data" v-if="current_weather" v-show="!loading && !error">
      <div class="data">
        <div class="data__location" v-if="city">
          {{ city }}
        </div>

        <div class="data__temperature">
          {{ Math.floor(current_weather.main.temp) }} {{ unitsSymbol }}
        </div>

        <div class="data__lowhigh">
          {{ temperatureLowHigh }}
        </div>

        <div class="data__description">
          <div class="data__description__icon">
            <img :src="currentWeatherIconUrl" />
          </div>
          <div class="data__description__text">
            {{ current_weather.weather[0].description }}
          </div>
        </div>
      </div>
    </div>

    <div class="widget__photo-attribution" v-if="image_creator">
      Photo by
        <a :href="image_creator.links.html + '?utm_source=Weather_Widget&utm_medium=referral'">
          {{image_creator.name}}
        </a>
        on <a href="https://unsplash.com?Weather_Widget&utm_medium=referral">Unsplash.</a>
    </div>
  </div>
</template>


<script>
import axios from 'axios';
import Unsplash, { toJson } from 'unsplash-js';

const countryByAbbreviation = require('country-json/src/country-by-abbreviation.json');

const unsplash = new Unsplash({
  applicationId: process.env.VUE_APP_UNSPLASH_KEY,
  secret: process.env.VUE_APP_UNSPLASH_SECRET,
});

export default {
  name: 'WeatherWidget',
  props: {
    units: {
      type: String,
      default: 'imperial',
      validator(value) {
        return ['imperial', 'celcius', 'kelvin'].indexOf(value) !== -1;
      },
    },
    initialZip: {
      type: String,
      default: '01890',
    },
    initialCountryCode: {
      type: String,
      default: 'US',
      validator(value) {
        return countryByAbbreviation
          .filter(country => country.abbreviation)
          .valueOf(value) !== -1;
      },
    },
    updateFrequency: {
      type: Number,
      default: 5000,
    },
  },
  data() {
    return {
      /* Core */
      geolocation_supported: false,
      error: null,
      input_focused: false,
      loading: true,
      updateWeatherInterval: null,

      /* Current Location */
      country: this.initialCountryCode,
      zip: this.initialZip,
      coordinates: {
        lat: null,
        lon: null,
      },

      /* Country Codes */
      countries: countryByAbbreviation,

      /* Weather Data */
      current_weather: null,
      city: null,

      /* Weather Style */
      current_weather_style: {},
      image_creator: null,
    };
  },
  mounted() {
    if (navigator.geolocation) {
      this.geolocation_supported = true;
    }

    this.updateWeather();
    this.updateWeatherInterval = setInterval(() => {
      this.updateWeather(false);
    }, this.updateFrequency);
  },
  beforeDestroy() {
    clearInterval(this.updateWeatherInterval);
  },
  computed: {
    // Temperature Units symbol
    unitsSymbol() {
      const degrees = '°';
      let symbol = '';

      if (this.units === 'imperial') {
        symbol = `${degrees}F`;
      } else if (this.units === 'celcius') {
        symbol = `${degrees}C`;
      } else if (this.units === 'kelvin') {
        symbol = `${degrees}K`;
      }

      return symbol;
    },

    // Current weather conditions icon
    currentWeatherIconUrl() {
      return `http://openweathermap.org/img/wn/${this.current_weather.weather[0].icon}.png`;
    },

    temperatureLowHigh() {
      const tempUnit = this.unitsSymbol;
      const low = Math.floor(this.current_weather.main.temp_min);
      const high = Math.floor(this.current_weather.main.temp_max);
      return `Low: ${low} ${tempUnit}  /  High: ${high} ${tempUnit}`;
    },
  },
  methods: {
    // Retrieve & save geolocation from browser, update weather from geolocation
    updateWeatherGeolocation() {
      this.getGeolocation()
        .then((position) => {
          this.loading = true;
          this.coordinates.lon = position.coords.longitude;
          this.coordinates.lat = position.coords.latitude;
          this.zip = null;
          this.city = null;
          this.updateWeather();
        })
        .catch(() => {
          this.error = 'Error retrieving weather from geolocation';
        });
    },

    // Update weather from zip and country
    updateWeatherZip() {
      if (this.zip && this.country) {
        this.loading = true;
        this.coordinates.lon = null;
        this.coordinates.lat = null;
        this.city = null;
        this.updateWeather();
      }
    },

    // Handle 'get current weather' response from OpenWeather API
    updateWeather(newLocation = true) {
      this.queryOpenWeatherAPI()
        .then((response) => {
          this.current_weather = response.data;
          this.error = '';
          this.city = response.data.name;
          if (newLocation) {
            this.queryUnsplashAPI();
          }
        })
        .catch(() => {
          this.error = 'Error: failed to retrieve weather data.';
          this.loading = false;
        });
    },

    // Promise wrapper for browser geolocation
    getGeolocation(options) {
      const supported = this.geolocation_supported;
      return new Promise(((resolve, reject) => {
        if (supported) {
          navigator.geolocation.getCurrentPosition(resolve, reject, options);
        } else {
          reject();
        }
      }));
    },

    // Query Unsplash api for random image of current weather condition & save
    queryUnsplashAPI() {
      unsplash.photos.getRandomPhoto({
        query: `${this.current_weather.weather[0].description} ${this.current_weather.weather[0].main} ${this.city} ${this.current_weather.sys.country}`,
      })
        .then(toJson)
        .then((json) => {
          this.$set(this.current_weather_style, 'background-image', `url(${json.urls.small})`);
          this.image_creator = json.user;
          this.loading = false;
        })
        .catch(() => {
          this.loading = false;
        });
    },

    // Send 'get current weather' request to OpenWeather API
    queryOpenWeatherAPI() {
      let promise;
      if (this.coordinates.lat && this.coordinates.lon) {
        promise = axios.get('https://api.openweathermap.org/data/2.5/weather', {
          params: {
            lat: this.coordinates.lat.toString(),
            lon: this.coordinates.lon.toString(),
            units: this.units,
            APPID: process.env.VUE_APP_OPENWEATHER_KEY,
          },
        });
      } else if (this.zip) {
        promise = axios.get('https://api.openweathermap.org/data/2.5/weather', {
          params: {
            q: `${this.zip},${this.country}`,
            units: this.units,
            APPID: process.env.VUE_APP_OPENWEATHER_KEY,
          },
        });
      }
      return promise;
    },
  },
};
</script>


<style lang="sass" scoped>
$widget-width: 400px
$widget-height: 220px

@keyframes bounce
  50%
    transform: translate(0px, -2px)
  70%
    transform: translate(0px, -1px)
  100%
    transform: translate(0px, 2px)

@keyframes spin
  100%
    transform: rotate(360deg)

.widget
  position: relative
  width: $widget-width
  height: $widget-height
  border-radius: 10px
  box-shadow: 0 10px 8px -8px black
  font-family: "sans-serif"

  &__error
    position: absolute
    bottom: 25px
    width: $widget-width - 10
    font-size: 0.7em
    text-align: center
    background-color: red
    color: white
    padding: 5px

  &__input
    z-index: 10
    display: none
    border-radius: 10px 10px 0px 0px

    &__geo
      z-index: 10
      margin: 10px 15px 10px 15px
      width: 15px
      color: lightgrey

      &:hover
        animation: bounce 0.5s
        animation-iteration-count: infinite

    &__zip
      z-index: 10
      width: 145px
      margin: 5px 10px 5px 10px
      float: right

      & input
        padding: 2px 4px 2px 4px
      & select
        padding: 1px

      &__element
        opacity: 0.4
        border: none
        outline: none
        box-shadow: none
        background-color: lightgrey


      select
        margin-left: 5px

    &--focused
      display: block

      & .widget__input__geo
        opacity: 1

      & .widget__input__zip__element
        opacity: 1
        box-shadow: none
        border: none
        outline: none

  &__data
    z-index: 5
    position: absolute
    top: 50%
    left: 50%
    transform: translate(-50%, -50%)
    text-align: center
    user-select: none

  &__photo-attribution
    position: absolute
    bottom: 0
    right: 0
    text-align: right
    font-size: 0.5em
    background: rgba(0, 0, 0, 0.6)
    padding: 5px 10px 5px 10px
    color: white
    border-radius: 15px 0px 15px 0px

    & a
      color: white
      text-decoration: none
      font-style: italic
      &:hover
        text-decoration: underline

.data
  z-index: 5
  background-color: rgba(0,0,0,0.5)
  color: white
  border-radius: 15px
  padding: 10px 10px 0px 10px

  &__location
    font-size: 2em
    margin-bottom: 5px

  &__temperature
    font-size: 2.5em
    font-weight: bold

  &__lowhigh
    font-size: 0.7em

  &__description
    display: flex
    justify-content: space-evenly
    align-items: center

.loader
  position: absolute
  top: 50%
  left: 50%
  transform: translate(-50%, -50%)
  color: lightgrey

  &--spinning
    animation: spin 1s
    animation-iteration-count: infinite

</style>
