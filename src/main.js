import Vue from 'vue';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faMapMarkerAlt, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// Vue Custom Element
import vueCustomElement from 'vue-custom-element';
import WeatherWidget from './components/WeatherWidget.vue';
import 'document-register-element/build/document-register-element';

library.add([faMapMarkerAlt, faSpinner]);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(vueCustomElement);
Vue.customElement('weather-widget', WeatherWidget);
