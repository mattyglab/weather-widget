// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

function mockGeo(lat, lon) {
  return `${'window.navigator.geolocation.getCurrentPosition = '
    + '       function (success, error) {'
    + '           var position = {'
    + '               "coords" : {'
    + '                   "latitude": "'}${lat}",`
    + `                   "longitude": "${lon}"`
    + '               }'
    + '           };'
    + '           success(position);'
    + '       }';
}

function mockGeoError(code) {
  return `${'window.navigator.geolocation.getCurrentPosition = '
    + '       function (success, error) {'
    + '           var err = {'
    + '               code: '}${code},`
    + '               PERMISSION_DENIED: 1,'
    + '               POSITION_UNAVAILABLE: 2,'
    + '               TIMEOUT: 3'
    + '           };'
    + '           error(err);'
    + '       }';
}

module.exports = {
  'initial temperature loaded': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.widget', 5000)
      .assert.elementPresent('.widget__data')
      .end();
  },
  'hover over widget displays input ': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.widget__data', 5000)
      .moveToElement('.widget', 0, 0)
      .assert.visible('.widget__input')
      .assert.visible('.widget__input__geo')
      .assert.visible('.widget__input__zip')
      .end();
  },
  'geolocation updates temperature': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .execute(mockGeo('42.3716776', '-71.22801179999999'))
      .waitForElementVisible('.widget__data', 5000)
      .moveToElement('.widget', 0, 0)
      .click('.widget__input__geo')
      .waitForElementVisible('.widget__data', 5000)
      .assert.containsText('.data__location', 'Waltham')
      .end();
  },
  'failed geolocation displays error': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .execute(mockGeoError(20))
      .waitForElementVisible('.widget__data', 5000)
      .moveToElement('.widget', 0, 0)
      .click('.widget__input__geo')
      .waitForElementVisible('.widget__error', 5000)
      .assert.containsText('.widget__error', 'Error')
      .end();
  },
  'zip updates temperature': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.widget', 5000)
      .moveToElement('.widget', 0, 0)
      .clearValue('.widget__input input')
      .setValue('.widget__input input', ['40003', browser.Keys.ENTER])
      .waitForElementVisible('.widget__data', 5000)
      .assert.containsText('.data__location', 'Frankfort')
      .end();
  },
  'Invalid zip displays error': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.widget', 5000)
      .moveToElement('.widget', 0, 0)
      .clearValue('.widget__input input')
      .setValue('.widget__input input', ['99999', browser.Keys.ENTER])
      .waitForElementVisible('.widget__error', 5000)
      .assert.containsText('.widget__error', 'Error')
      .end();
  },
  'Country updates temperature': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.widget', 5000)
      .moveToElement('.widget', 0, 0)
      .clearValue('.widget__input input')
      .setValue('.widget__input input', 'WC2N')
      .click('.widget__input select option[value=GB]')
      .waitForElementVisible('.widget__data', 5000)
      .assert.containsText('.data__location', 'London')
      .end();
  },
  'Invalid country displays error': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.widget', 5000)
      .moveToElement('.widget', 0, 0)
      .click('.widget__input select option[value=TG]')
      .waitForElementVisible('.widget__error', 5000)
      .assert.containsText('.widget__error', 'Error')
      .end();
  },
};
